#include <sys/shm.h>
#include <sys/futex.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <unistd.h>

#define CYCLES 100000000LL

int main(int argc, char** argv)
{
	int shmid;
	void *seg;
	futex_t *f;
	volatile long long int *val;
	int i;
	int nproc;
	volatile long long int temp;
	pid_t pid;

	if (argc != 3) {
		printf("usage: %s <id> <nproc>\n", argv[0]);
		return 0;
	}

	nproc = atoi(argv[2]);

	shmid = shmget(IPC_PRIVATE,
			sizeof(futex_t) + sizeof(long long int), 0666|IPC_CREAT);
	if (shmid < 0) return 1;
	if ((seg = shmat(shmid, 0, 0)) == NULL) {
		printf("shmat failed\n");
		return 1;
	}
	f = (futex_t*)seg;
	val = (long long int*)seg + sizeof(futex_t);

	if (futex_init(f)) {
		printf("futex_init failed\n");
		return 1;
	}

	for (i = 1; i < nproc; ++i) {
		if ((pid = fork()) < 0) {
			printf("fork failed\n");
			return 1;
		}
		if (pid == 0) break;
	}

	for (i = 0; i < CYCLES; ++i)
	{
		if (futex_lock(f)) {
			printf("futex_lock failed\n");
			return 1;
		}
		temp = *val;
		temp++;
		*val = temp;
		if (futex_unlock(f)) {
			printf("futex_unlock failed\n");
			return 1;
		}
	}

	if (pid == 0) return 0;

	for (i = 1; i < nproc; ++i)
		wait(NULL);

	fprintf(stderr, "Value #%s: %lld\n", argv[1], *val);
	temp = *val;

	if (futex_destroy(f)) {
		printf("futex_destroy failed\n");
		return 1;
	}

	if (shmdt(seg)) {
		printf("shmdt failed\n");
		return 1;
	}
	if (shmctl(shmid, IPC_RMID, 0)) {
		printf("shmctl failed\n");
		return 1;
	}
	if (temp != CYCLES * nproc) {
		printf("wrong cycle count\n");
		return 1;
	}
	return 0;
}

