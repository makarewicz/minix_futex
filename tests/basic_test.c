#include <sys/futex.h>
#include <stdio.h>

futex_t f;

int main(int argc, char** argv)
{
int r;
puts("init");
	if (r = futex_init(&f))
		return r;
puts("lock");
	if (r = futex_lock(&f))
		return r;
puts("unlock");
	if (r = futex_unlock(&f))
		return r;
puts("destroy");
	if (r = futex_destroy(&f))
		return r;
	return 0;
}
