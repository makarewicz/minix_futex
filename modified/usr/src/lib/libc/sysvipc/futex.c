/* ---------------------------------------------------------------------------
** futex.c
**
** Author: Michal Makarewicz (michal.makarewicz1@gmail.com)
** -------------------------------------------------------------------------*/
#define _SYSTEM 1
#define _MINIX 1

#include <minix/com.h>
#include <lib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/futex.h>
#include <stdio.h>

/**
 * Gets endpointer of ipc server.
 */
PRIVATE int get_ipc_endpt(endpoint_t *pt) {
	return minix_rs_lookup("ipc", pt);
}

/**
 * Futex initialize function.
 */
PUBLIC int futex_init(futex_t *f) {
	endpoint_t ipc_pt;
	message m;
	int r;
	if (get_ipc_endpt(&ipc_pt) != OK) {
		errno = ENOSYS;
		return -1;
	}
	r = _syscall(ipc_pt, IPC_FUTINIT, &m);
	if (r != OK)
		return r;
	f->id = m.FUTINIT_ID;
	f->val = 0;
	return OK;
}

/**
 * Futex destroy function.
 */
PUBLIC int futex_destroy(futex_t *f) {
	endpoint_t ipc_pt;
	message m;
	if (get_ipc_endpt(&ipc_pt) != OK) {
		errno = ENOSYS;
		return -1;
	}
	m.FUTDESTROY_ID = f->id;
	return _syscall(ipc_pt, IPC_FUTDESTROY, &m);
}

/**
 * Wake up n processes waiting for futex f, usually we only wake up 1.
 */
PRIVATE int futex_wake(futex_t *f, int n) {
	endpoint_t ipc_pt;
	message m;
	if (get_ipc_endpt(&ipc_pt) != OK) {
		errno = ENOSYS;
		return -1;
	}
	m.FUTWAKE_ID = f->id;
	m.FUTWAKE_N = n;
	return _syscall(ipc_pt, IPC_FUTWAKE, &m);
}

/**
 * If the value of the futex (f->val) equals val, wait until woken by a signal or a call.
 * If values are not equal, function returns OK without waiting.
 */
PRIVATE int futex_wait(futex_t *f, int val) {
	endpoint_t ipc_pt;
	message m;
	if (get_ipc_endpt(&ipc_pt) != OK) {
		errno = ENOSYS;
		return -1;
	}
	m.FUTWAIT_ID = f->id;
	m.FUTWAIT_VAL = val;
	m.FUTWAIT_VALPTR = (void*)&f->val;
	return _syscall(ipc_pt, IPC_FUTWAIT, &m);
}

/**
 * Futex lock function.
 * Function waits for getting futex, or futex being destroy (in which case
 * error code is returned).
 */
PUBLIC int futex_lock(futex_t *f) {
	int c;
	int r = OK;
	if ((c = __sync_val_compare_and_swap(&f->val, 0, 1)) != 0) {
		if (c != 2)
			c =  __sync_lock_test_and_set(&f->val, 2);
		while (c != 0) {
			r = futex_wait (f, 2);
			if (r != OK) {
				/* Futex has been destroyed, or there was an error with waiting. */
				return r;
			}
			c = __sync_lock_test_and_set(&f->val, 2);
		}
	}
	return r;
}

/**
 * Futex unlock function.
 */
PUBLIC int futex_unlock(futex_t *f) {
	if (__sync_fetch_and_sub(&f->val, 1) != 1) {
		f->val = 0;
		return futex_wake(f, 1);
	}
	return OK;
}
