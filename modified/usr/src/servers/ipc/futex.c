/* ---------------------------------------------------------------------------
** futex.c
**
** Author: Michal Makarewicz (michal.makarewicz1@gmail.com)
** -------------------------------------------------------------------------*/
#include "inc.h"

struct fut_waiting {
	endpoint_t who;
};

struct fut_struct {
    /**
     * Id shared with library futex.
     * Id always equals index in fut_list, it is kept here for simplicity.
     */
	int id;

    /* Number of processes waiting for futex. */
	int futcnt;
    
    /* List of waiting processes. */
	struct fut_waiting* list;
};

/* List of futexes in system. */
PRIVATE struct fut_struct* fut_list[FUTMAX];
PRIVATE int fut_list_nr = 0;

/**
 * Finds first free futex in fut_list and initialize it.
 */
PRIVATE struct fut_struct* fut_find_first_free_and_init() {
	int i;
    /* Checking if we can reuse space after destroyed futex. */
	for (i = 0; i < fut_list_nr; i++) {
		if (fut_list[i] == NULL) {
			fut_list[i] = malloc(sizeof(struct fut_struct));
			if (!fut_list[i]) {
				return NULL;
			}
			fut_list[i]->id = i;
			fut_list[i]->futcnt = 0;
			fut_list[i]->list = NULL;
			return fut_list[i];
		}
	}
	if (fut_list_nr < FUTMAX) {
		fut_list[fut_list_nr] = malloc(sizeof(struct fut_struct));
		if (!fut_list[fut_list_nr]) {
			return NULL;
		}
		fut_list[fut_list_nr]->id = fut_list_nr;
		fut_list[fut_list_nr]->futcnt = 0;
		fut_list[fut_list_nr]->list = NULL;
		fut_list_nr++;
		return fut_list[fut_list_nr - 1];
	}
	return NULL;
}

/* Sends message to process with given return value. */
PRIVATE void send_message_to_process(endpoint_t who, int ret) {
	message m;

	m.m_type = ret;
	sendnb(who, &m);
}

/* Initialize futex on kernel side. */
PUBLIC int do_futinit(message* m) {
	struct fut_struct* fut;

	fut = fut_find_first_free_and_init();
	if (!fut) {
		return ENOMEM;
	}
	m->FUTINIT_ID = fut->id;
	return OK;
}

/**
 * Wake up up to n processes waiting for futex.
 * If remove is != 0, error code is returned by futex_wait.
 */
PRIVATE void wake_up_processes(struct fut_struct *fut, int n, int remove) {
	int i;
	int r;
	if (remove) {
		r = EINTR;
	} else {
		r = OK;
	}
	if (n > fut->futcnt) {
		n = fut->futcnt;
	}
	for(i = 0; i < n; ++i) {
		send_message_to_process(fut->list[i].who, r);
	}
	memmove(fut->list, fut->list + n, sizeof(struct fut_waiting) * (fut->futcnt - n));
	fut->futcnt -= n;
}

/**
 * Destroy futex with given id on kernel side.
 */
PUBLIC int do_futdestroy(message* m) {
	int id;
	id = m->FUTDESTROY_ID;
	if (id >= FUTMAX || fut_list[id] == NULL) {
		return EINVAL;
	}
	wake_up_processes(fut_list[id], INT_MAX, 1);
	free(fut_list[id]);
	fut_list[id] = NULL;
	return OK;
}

/**
 * If futex value hasn't changed wait on futex.
 */
PUBLIC int do_futwait(message* m) {
	int id;
	int val;
	void* valptr;
	int real_val;
	int r;
	struct fut_struct* fut;
	
	id = m->FUTWAIT_ID;
	val = m->FUTWAIT_VAL;
	valptr = m->FUTWAIT_VALPTR;
    /* Copying actual value of futex. */
	r = sys_datacopy(who_e, (vir_bytes) valptr,
			SELF_E, (vir_bytes) &real_val, sizeof(int));
	if (r != OK) {
		r = ENOSYS;
		goto out;
	}
	/* If real_val != val we don't lock process, but still return OK */
	if (real_val != val) {
		m->m_type = OK;
		sendnb(who_e, m);
		return OK;
	}
	if (id >= FUTMAX || !(fut = fut_list[id])) {
		r = EINVAL;
		goto out;
	}

	fut->futcnt++;
	fut->list = realloc(fut->list, sizeof(struct fut_waiting) * fut->futcnt);

	fut->list[fut->futcnt - 1].who = who_e;
	r = OK;

out:
    /* If presented with error, wake up process and return error code. */
	if (r != OK) {
		m->m_type = r;
		sendnb(who_e, m);
	}

	return r;
}

/**
 * Wake up up to n processes waiting for futex.
 */
PUBLIC int do_futwake(message* m) {
	int id;
	int n;
	struct fut_struct* fut;

	id = m->FUTWAKE_ID;
	n = m->FUTWAKE_N;
	if (id >= FUTMAX || !(fut = fut_list[id])) {
		return EINVAL;
	}
	wake_up_processes(fut, n, 0);
	return OK;
}
