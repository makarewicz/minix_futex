/* ---------------------------------------------------------------------------
** futex.h
**
** Author: Michal Makarewicz (michal.makarewicz1@gmail.com)
** -------------------------------------------------------------------------*/
#ifndef _SYS_FUTEX_H
#define _SYS_FUTEX_H

/* Maximal number of futexes in system. */
#define FUTMAX 4096

#include <sys/types.h>

typedef struct
{
	/**
	 * State of the futex:
	 * 0 - unlocked,
	 * 1 - locked, no waiters,
	 * 2 - locked, one or more waiters.
	 */
	int val;
	/* Futex id in kernel. */
	int id;
} futex_t;

/* Futex initialize and destroy functions  */
_PROTOTYPE( int futex_init, (futex_t *futex));
_PROTOTYPE( int futex_destroy, (futex_t *futex));

/* Lock and unlock futex */
_PROTOTYPE( int futex_lock, (futex_t *futex));
_PROTOTYPE( int futex_unlock, (futex_t *futex));


#endif /* _SYS_FUTEX_H */
